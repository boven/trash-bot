from json import loads
from os import path
from os import remove
from urllib import request
import datetime


class MyMuell:
    def __init__(self):
        self.today = datetime.datetime.now().date()
        self.delta = 0
        self.notification_date = self.today + datetime.timedelta(days=self.delta)
        self.str_notification_date = str(self.notification_date)
        self.data = ""
        self.data_list = []
        #immer einen tag vorher rausstellen
        self.day_names ={0:"tja", 1:"Heute", 2:"Morgen", 3:"Übermorgen", 4:"In drei Tagen"}
        self.trash_types = {'BIO': 'grüne Tonne', 'REST': 'schwarze Tonne', 'PT': "blaue Tonne", 'ASP_WERTT': 'gelbe Tonne'}
        self.room_mates = ['Rene', 'Marius', 'Merlin', 'Andre', 'Jenny', 'Tim', 'Lennard', 'Claudia', 'Milena', 'Siggi']
        self.tonnen = []
        
    def choose_roommate(self):
        with open('/home/pi/whatapp_web_cli_chrome/order') as file:
            responsible = self.room_mates[int(file.read())]
        return responsible
    
    def set_next_roommate(self):
        with open('/home/pi/whatapp_web_cli_chrome/order') as file:
            number = int(file.read())
            if number < 9:
                number += 1
                print("increasing number: ", number)
            else:
                number = 0
                print("number is zero : ", number)
        with open('/home/pi/whatapp_web_cli_chrome/order', 'w') as file:
            file.write(str(number))
        
    def load_url(self, link):
        with request.urlopen(link) as url:
            json_data = url.read().decode()
            self.write_string(data=json_data)
            self.data = loads(json_data)
            self.data_list = self.data[0]['_data']

    def load_file(self, mypath="/home/pi/whatapp_web_cli_chrome/", name='muell_data.json'):
        with open(mypath+name) as file:
            self.data = loads(file.read())
            self.data_list = self.data[0]['_data']

    def write_string(self, location='', name='muell_data.json', data=""):
        with open(location+name, 'w+') as file:
            file.write(str(data))
    def find_next_schedule(self):
        for key in range(5):
            next_date = self.today + datetime.timedelta(days=key)
            t_list = self.filter_data(next_date)
            if len(t_list) > 0:
                return {"date":next_date, "delta":key, "trash":t_list}
        return None

    def create_message(self):
        room_mate = ""
        day_name="BAMJUNGE"
        schedule_data = self.find_next_schedule()
        notification_date = schedule_data["date"]
        if self.today == notification_date:
            self.set_next_roommate()
            return
        self.str_notification_date = str(notification_date)
        t_list = schedule_data["trash"]
        print("nd", notification_date, self.today, t_list)
        if len(t_list) > 0:
        #   print(t_list, "dd")
        #set next roommate only on day, when trash is collected
            self.tonnen = t_list
            delta = schedule_data["delta"]
            print("Tonnen: ",self.tonnen)
            day_name = self.day_names[delta]
            room_mate = self.choose_roommate();
            print("roommate: ", room_mate)
                
        message = ""
        if len(self.tonnen) == 1:
            message = '{2} muss {0} die {1} rausstellen.'.format(room_mate, self.tonnen[0], day_name)
        elif len(self.tonnen) == 2:
            message = '{3} muss {2} die {0} und die {1} rausstellen.'.format(self.tonnen[0], self.tonnen[1], room_mate, day_name)
        elif len(self.tonnen) == 3:
            message = '{4} muss {3} die {0}, {1} und die {2} rausstellen.'.format(self.tonnen[0], self.tonnen[1], self.tonnen[2], room_mate, day_name)
        elif len(self.tonnen) == 4:
            message = '{5} muss {4} die {0}, {1}, {2} und die {3} rausstellen.'.format(self.tonnen[0], self.tonnen[1], self.tonnen[2], self.tonnen[3], room_mate, day_name)
        print(message)
        if len(self.tonnen) > 0:
            self.write_string(name="/home/pi/whatapp_web_cli_chrome/message", data=message)

    def filter_data(self, day):
        t_list = []
        for i in self.data_list:
            if str(day) == i['cal_date']:
                tonnenart = i['cal_garbage_type']
                if tonnenart in self.trash_types.keys():
                    t_list.append(self.trash_types[tonnenart])
        self.write_string(data=str(self.data_list))
        return t_list


def main():
    if path.exists("/home/pi/whatapp_web_cli_chrome/message"):
        remove("/home/pi/whatapp_web_cli_chrome/message")
    muell = MyMuell()
    link = 'https://mymuell.jumomind.com/webservice.php?idx=termins&city_id=48001&area_id=986&ws=3'
    muell.load_url(link)
    muell.create_message()



if __name__ == "__main__":
    main()
